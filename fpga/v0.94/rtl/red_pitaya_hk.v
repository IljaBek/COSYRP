/**
 * $Id: red_pitaya_hk.v 961 2014-01-21 11:40:39Z matej.oblak $
 *
 * @brief Red Pitaya house keeping.
 *
 * @Author Matej Oblak
 *
 * (c) Red Pitaya  http://www.redpitaya.com
 *
 * This part of code is written in Verilog hardware description language (HDL).
 * Please visit http://en.wikipedia.org/wiki/Verilog
 * for more details on the language used herein.
 */

/**
 * GENERAL DESCRIPTION:
 *
 * House keeping module takes care of system identification.
 *
 *
 * This module takes care of system identification via DNA readout at startup and
 * ID register which user can define at compile time.
 *
 * Beside that it is currently also used to test expansion connector and for
 * driving LEDs.
 * 
 */

module red_pitaya_hk #(
  parameter ScLED = 2, // LEDs used in Scope
  parameter DWL = 8-ScLED, // data width for LED
  parameter DWE = 8, // data width for extension
  parameter [57-1:0] DNA = 57'h0823456789ABCDE,
  parameter [32-1:0] FW  = 32'd2017042514  // 32'7839a052
)(
  // system signals
  input                clk_i      ,  // clock
  input                rstn_i     ,  // reset - active low
  // Slow clock (23rd bit, 67ms)
  output               slw_clk_o  ,
  // LED
  output     [DWL-1:0] led_o      ,  // LED output
  // global configuration
  output reg           digital_loop,
  // Expansion connector
  input      [DWE-1:0] exp_p_dat_i,  // exp. con. input data
  output reg [DWE-1:0] exp_p_dat_o,  // exp. con. output data
  output reg [DWE-1:0] exp_p_dir_o,  // exp. con. 1-output enable
  input      [DWE-1:0] exp_n_dat_i,  //
  output reg [DWE-1:0] exp_n_dat_o,  //
  output reg [DWE-1:0] exp_n_dir_o,  //
  // System bus
  input      [ 32-1:0] sys_addr   ,  // bus address
  input      [ 32-1:0] sys_wdata  ,  // bus write data
  input                sys_wen    ,  // bus write enable
  input                sys_ren    ,  // bus read enable
  output reg [ 32-1:0] sys_rdata  ,  // bus read data
  output reg           sys_err    ,  // bus error indicator
  output reg           sys_ack       // bus acknowledge signal
);




//---------------------------------------------------------------------------------
//
//  FW version identification


wire [32-1: 0] id_firmware ;// FW version
assign id_firmware[32-1:  0] = FW;

reg  [ 32-1: 0] id_import ;

//---------------------------------------------------------------------------------
//
//  Clock Counter

reg  [ 32-1: 0] clk_cnt ;

always @(posedge clk_i) begin
   if (rstn_i == 1'b0) begin
      clk_cnt    <= 32'h0 ;
   end
   else begin
      clk_cnt    <= clk_cnt + 32'h1 ;
   end
end

reg [2-1: 0] slw_clk_reg ;
wire slw_clk_long;
assign slw_clk_long = clk_cnt[22];

always @(posedge clk_i) begin
   if (rstn_i == 1'b0) begin
      slw_clk_reg    <= 2'h0 ;
   end
   else begin
      slw_clk_reg    <= {slw_clk_reg[0] , slw_clk_long} ;
   end
end

assign slw_clk_o  = slw_clk_long & (slw_clk_long ^ slw_clk_reg[1]); //clk_cnt[22]  ;


//---------------------------------------------------------------------------------
//
//  Trigger Counter: GPIO p+n x8
//

// 02-04-06-08-10-12-14-16-18-20-22-24-26
// 01-03-05-07-09-11-13-15-17-19-21-23-25
//     0  1  2  3  4  5  6  7          GND


reg  [ 32-1: 0] exp_p_cnt [DWE-1:0]; // Counters
reg  [  2-1: 0] exp_p_shr [DWE-1:0]; // shift registers
reg             exp_p_trg [DWE-1:0]; // triggered edge
reg  [ 32-1: 0] exp_n_cnt [DWE-1:0]; // Counters
reg  [  2-1: 0] exp_n_shr [DWE-1:0]; // shift registers
reg             exp_n_trg [DWE-1:0]; // triggered edge

reg   [DWE-1:0] exp_p_edge         ; // edge [7..0] ; 0=falling("10") 1=rising("01")
reg   [DWE-1:0] exp_n_edge         ; // edge [15..8]; 0=falling("10") 1=rising("01")

reg             rst_cnt            ; //counters reset

//genvar i;
generate for (genvar i = 0; i < DWE ; i = i + 1) begin : exp_p_8
always @(posedge clk_i) begin
   if (rstn_i == 1'b0 | rst_cnt == 1'b1) begin
         exp_p_cnt[i] <= 32'h0 ;
         exp_p_shr[i] <=  2'h0 ;
         exp_n_cnt[i] <= 32'h0 ;
         exp_n_shr[i] <=  2'h0 ;
   end
   else begin

   /// shift registers to catch "10" edge. 
   exp_p_shr[i] <= {exp_p_shr[i][0],exp_p_dat_i[i]}; 
   exp_n_shr[i] <= {exp_n_shr[i][0],exp_n_dat_i[i]}; 
   
   if ( exp_p_edge[i] == 1'b0 ) begin
      if ( exp_p_shr[i] == 2'b10 ) exp_p_cnt[i] = exp_p_cnt[i] + 32'h1 ;
   else
      if ( exp_p_shr[i] == 2'b01 ) exp_p_cnt[i] = exp_p_cnt[i] + 32'h1 ;
   end
   

   if ( exp_n_edge[i] == 1'b0 ) begin
      if ( exp_n_shr[i] == 2'b10 ) exp_n_cnt[i] = exp_n_cnt[i] + 32'h1 ;
   else
      if ( exp_n_shr[i] == 2'b01 ) exp_n_cnt[i] = exp_n_cnt[i] + 32'h1 ;
   end
   
   end
end
end
endgenerate

//---------------------------------------------------------------------------------
//
// Counter Capturing/Keeping/Saving 
reg  [ 32-1: 0] clk_cnt_keep ;
reg  [ 32-1: 0] exp_p_cnt_keep [DWE-1:0]; // Counters
reg  [ 32-1: 0] exp_n_cnt_keep [DWE-1:0]; // Counters

reg  keep ; // guards saving of counters
/// TODO: convert/check to synchronous
always @(posedge keep) begin
   if (rstn_i == 1'b0 | rst_cnt == 1'b1) begin
      clk_cnt_keep <= 32'h0 ;
      exp_p_cnt_keep[0] <= 32'h0;
      exp_p_cnt_keep[1] <= 32'h0;
      exp_p_cnt_keep[2] <= 32'h0;
      exp_p_cnt_keep[3] <= 32'h0;
      exp_p_cnt_keep[4] <= 32'h0;
      exp_p_cnt_keep[5] <= 32'h0;
      exp_p_cnt_keep[6] <= 32'h0;
      exp_p_cnt_keep[7] <= 32'h0;
      exp_n_cnt_keep[0] <= 32'h0;
      exp_n_cnt_keep[1] <= 32'h0;
      exp_n_cnt_keep[2] <= 32'h0;
      exp_n_cnt_keep[3] <= 32'h0;
      exp_n_cnt_keep[4] <= 32'h0;
      exp_n_cnt_keep[5] <= 32'h0;
      exp_n_cnt_keep[6] <= 32'h0;
      exp_n_cnt_keep[7] <= 32'h0;
   end
   else if (keep == 1'b1) begin
      clk_cnt_keep <= clk_cnt;
      exp_p_cnt_keep[0] <= exp_p_cnt[0];
      exp_p_cnt_keep[1] <= exp_p_cnt[1];
      exp_p_cnt_keep[2] <= exp_p_cnt[2];
      exp_p_cnt_keep[3] <= exp_p_cnt[3];
      exp_p_cnt_keep[4] <= exp_p_cnt[4];
      exp_p_cnt_keep[5] <= exp_p_cnt[5];
      exp_p_cnt_keep[6] <= exp_p_cnt[6];
      exp_p_cnt_keep[7] <= exp_p_cnt[7];
      exp_n_cnt_keep[0] <= exp_n_cnt[0];
      exp_n_cnt_keep[1] <= exp_n_cnt[1];
      exp_n_cnt_keep[2] <= exp_n_cnt[2];
      exp_n_cnt_keep[3] <= exp_n_cnt[3];
      exp_n_cnt_keep[4] <= exp_n_cnt[4];
      exp_n_cnt_keep[5] <= exp_n_cnt[5];
      exp_n_cnt_keep[6] <= exp_n_cnt[6];
      exp_n_cnt_keep[7] <= exp_n_cnt[7];
   end
end

wire keep_led;

red_pitaya_led i_led_a (
  .trig_i  ( keep            ), 
  .clk_i   ( clk_cnt[22]     ),
  .rstn_i  ( rstn_i          ),
  .led_o   ( keep_led  )
);



//---------------------------------------------------------------------------------
//
// LED blinking
reg  [ DWL-1: 0] led_reg ;      

always @(posedge clk_i) begin
   if (rstn_i == 1'b0) begin
      led_reg <=  {DWL{1'b0}} ;
   end
   else begin
      led_reg <= { slw_clk_o , keep_led, {DWL-2{1'b0}} };
   end
end


assign led_o = led_reg ;



//---------------------------------------------------------------------------------
//
//  Read device DNA

wire           dna_dout ;
reg            dna_clk  ;
reg            dna_read ;
reg            dna_shift;
reg  [ 9-1: 0] dna_cnt  ;
reg  [57-1: 0] dna_value;
reg            dna_done ;

always @(posedge clk_i)
if (rstn_i == 1'b0) begin
  dna_clk   <=  1'b0;
  dna_read  <=  1'b0;
  dna_shift <=  1'b0;
  dna_cnt   <=  9'd0;
  dna_value <= 57'd0;
  dna_done  <=  1'b0;
end else begin
  if (!dna_done)
    dna_cnt <= dna_cnt + 1'd1;

  dna_clk <= dna_cnt[2] ;
  dna_read  <= (dna_cnt < 9'd10);
  dna_shift <= (dna_cnt > 9'd18);

  if ((dna_cnt[2:0]==3'h0) && !dna_done)
    dna_value <= {dna_value[57-2:0], dna_dout};

  if (dna_cnt > 9'd465)
    dna_done <= 1'b1;
end

// parameter specifies a sample 57-bit DNA value for simulation
DNA_PORT #(.SIM_DNA_VALUE (DNA)) i_DNA (
  .DOUT  ( dna_dout   ), // 1-bit output: DNA output data.
  .CLK   ( dna_clk    ), // 1-bit input: Clock input.
  .DIN   ( 1'b0       ), // 1-bit input: User data input pin.
  .READ  ( dna_read   ), // 1-bit input: Active high load DNA, active low read input.
  .SHIFT ( dna_shift  )  // 1-bit input: Active high shift enable input.
);

//---------------------------------------------------------------------------------
//
//  Desing identification

wire [32-1: 0] id_value;

assign id_value[31: 4] = 28'h0; // reserved
assign id_value[ 3: 0] =  4'h1; // board type   1 - release 1

//---------------------------------------------------------------------------------
//
//  System bus connection

always @(posedge clk_i)
if (rstn_i == 1'b0) begin
//  led_o        <= {DWL{1'b0}};
  keep         <= 1'b0;
  rst_cnt      <= 1'b0;
  exp_p_dat_o  <= {DWE{1'b0}};
  exp_p_dir_o  <= {DWE{1'b0}};
  exp_n_dat_o  <= {DWE{1'b0}};
  exp_n_dir_o  <= {DWE{1'b0}};
  exp_p_edge   <= {DWE{1'b0}};
  exp_n_edge   <= {DWE{1'b0}};
  id_import    <= 32'h0 ;
end else if (sys_wen) begin
  if (sys_addr[19:0]==20'h0000C)   digital_loop <= sys_wdata[0];

  if (sys_addr[19:0]==20'h00100)   keep <= sys_wdata[0];
  if (sys_addr[19:0]==20'h00104)   rst_cnt <= sys_wdata[0];

  if (sys_addr[19:0]==20'h00200)   {exp_n_edge,exp_p_edge} <= sys_wdata[2*DWE-1:0];
  

//  if (sys_addr[19:0]==20'h10)   exp_p_dir_o  <= sys_wdata[DWE-1:0];
//  if (sys_addr[19:0]==20'h14)   exp_n_dir_o  <= sys_wdata[DWE-1:0];
//  if (sys_addr[19:0]==20'h18)   exp_p_dat_o  <= sys_wdata[DWE-1:0];
//  if (sys_addr[19:0]==20'h1C)   exp_n_dat_o  <= sys_wdata[DWE-1:0];

//  if (sys_addr[19:0]==20'h30)   led_o        <= sys_wdata[DWL-1:0];
  if (sys_addr[19:0]==20'hFFFF4)   id_import    <= sys_wdata[32-1: 0];
end

wire sys_en;
assign sys_en = sys_wen | sys_ren;

always @(posedge clk_i)
if (rstn_i == 1'b0) begin
  sys_err <= 1'b0;
  sys_ack <= 1'b0;
end else begin
  sys_err <= 1'b0;

  casez (sys_addr[19:0])
    20'h00000: begin sys_ack <= sys_en;  sys_rdata <= {                id_value          }; end
    20'h00004: begin sys_ack <= sys_en;  sys_rdata <= {                dna_value[32-1: 0]}; end
    20'h00008: begin sys_ack <= sys_en;  sys_rdata <= {{64- 57{1'b0}}, dna_value[57-1:32]}; end
    20'h0000c: begin sys_ack <= sys_en;  sys_rdata <= {{32-  1{1'b0}}, digital_loop      }; end

    20'h00010: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWE{1'b0}}, exp_p_dir_o}       ; end
    20'h00014: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWE{1'b0}}, exp_n_dir_o}       ; end
    20'h00018: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWE{1'b0}}, exp_p_dat_o}       ; end
    20'h0001C: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWE{1'b0}}, exp_n_dat_o}       ; end
    20'h00020: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWE{1'b0}}, exp_p_dat_i}       ; end
    20'h00024: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWE{1'b0}}, exp_n_dat_i}       ; end

    //led_o
    20'h00030: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWL{1'b0}}, led_reg}           ; end

    20'h00034: begin sys_ack <= sys_en;  sys_rdata <= {              clk_cnt      }       ; end //obsoleting

    20'h00100: begin sys_ack <= sys_en;  sys_rdata <= {{32- 1{1'b0}}, keep        }       ; end
    20'h00104: begin sys_ack <= sys_en;  sys_rdata <= {              clk_cnt_keep }       ; end //obsoleting

    20'h00200: begin sys_ack <= sys_en;  sys_rdata <= {{32-DWL*2{1'b0}} , exp_n_edge , exp_p_edge }       ; end

    20'h00FF0: begin sys_ack <= sys_en;  sys_rdata <= {              clk_cnt_keep }       ; end

    20'h01000: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[0] }       ; end
    20'h01004: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[1] }       ; end
    20'h01008: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[2] }       ; end
    20'h0100C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[3] }       ; end

    20'h01010: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[4] }       ; end
    20'h01014: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[5] }       ; end
    20'h01018: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[6] }       ; end
    20'h0101C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt[7] }       ; end

    20'h01020: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[0] }       ; end
    20'h01024: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[1] }       ; end
    20'h01028: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[2] }       ; end
    20'h0102C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[3] }       ; end

    20'h01030: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[4] }       ; end
    20'h01034: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[5] }       ; end
    20'h01038: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[6] }       ; end
    20'h0103C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt[7] }       ; end

    20'h01100: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[0] }  ; end
    20'h01104: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[1] }  ; end
    20'h01108: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[2] }  ; end
    20'h0110C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[3] }  ; end

    20'h01110: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[4] }  ; end
    20'h01114: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[5] }  ; end
    20'h01118: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[6] }  ; end
    20'h0111C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_p_cnt_keep[7] }  ; end

    20'h01120: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[0] }  ; end
    20'h01124: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[1] }  ; end
    20'h01128: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[2] }  ; end
    20'h0112C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[3] }  ; end

    20'h01130: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[4] }  ; end
    20'h01134: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[5] }  ; end
    20'h01138: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[6] }  ; end
    20'h0113C: begin sys_ack <= sys_en;  sys_rdata <= {              exp_n_cnt_keep[7] }  ; end

    20'hFFFF0: begin sys_ack <= sys_en;  sys_rdata <= {                   clk_cnt }       ; end

    20'hFFFF4: begin sys_ack <= sys_en;  sys_rdata <= {              id_import    }       ; end
    20'hFFFFC: begin sys_ack <= sys_en;  sys_rdata <= {              id_firmware  }       ; end

      default: begin sys_ack <= sys_en;  sys_rdata <=  32'h0                              ; end
  endcase
end

endmodule


//-------------


/*
red_pitaya_led i_led_a (
  .trig_i  ( adc_trig_a      ), 
  .clk_i   ( slw_clk_i       ),
  .rstn_i  ( adc_rstn_i      ),
  .led_o   ( adc_trig_a_led  )
);
*/

module red_pitaya_led (
   input                    trig_i       ,  // trigger input
   input                     clk_i       ,  // slow clock
   input                    rstn_i       ,  // reset n
   output                    led_o          // LED out

);

//---------------------------------------------------------------------------------
// LED driver for event edges
// Latch -> ShiftReg to catch and prolong fast signals wrt to slow LED clock slw_clk

localparam LEDShW = 4; //shift reg width for LED driver, must be at least 2; delay = (LEDShW-1)*slw_clk

reg  [ LEDShW-1: 0] trig_shr ; //shift reg A

wire trig_led; //output

wire all_event; //edge sensitive to all of these
//assign all_event = clk_i | trig_i | !rstn_i ;
//assign all_event = (clk_i ^ trig_i) | !rstn_i ; // clock | trig obscures posedges
assign all_event = trig_i | !rstn_i ;

/// Async-Latch A
//always @( posedge all_event )
always @( posedge clk_i or posedge trig_i)
   if (trig_i == 1'b0) begin
      trig_shr[0] <= 0; // reset
   end else begin // enable
      trig_shr[0] <= trig_i; // set
   end


/// shift register A
always @(posedge clk_i) begin
   trig_shr[LEDShW-1: 1] <= { trig_shr[LEDShW-1-1: 0], trig_shr[0] }; 
end

/// LED output as reduct. OR of the shift register
assign led_o = |trig_shr[LEDShW-1: 0];

endmodule





/*
red_pitaya_led_v1 i_led_a (
  .trig_i  ( adc_trig_a      ), 
  .clk_i   ( slw_clk_i       ),
  .rstn_i  ( adc_rstn_i      ),
  .led_o   ( adc_trig_a_led  )
);
*/

/*
module red_pitaya_led_v01 (
   input                    trig_i        ,  // trigger input
   input                    clk_i         ,  // full clock
   input          [32-1:0]  clk_cnt       ,  // full clock
   input                    rstn_i        ,  // reset n
   output                   led_o            // LED out

);

//---------------------------------------------------------------------------------
// LED driver for event edges
// save current clock in a reg and compare with MSB flipped
localparam CLKBIT = 25;

reg  [ CLKBIT-1: 0] comp ; //comparing register

wire trig_led; //output

always @(posedge trig_i)
   if (rstn_i == 1'b0) begin
      comp <= { CLKBIT{1'b0} }; // reset
   end else begin // enable
      comp <= { ~ clk_cnt[CLKBIT] , clk_cnt[CLKBIT-1-1:0] };
   end

assign trig_led = (comp == clk_cnt[CLKBIT-1:0]);


endmodule

*/