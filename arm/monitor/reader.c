/**
 * $Id$
 *
 * @brief Simple program to read/write from/to any location in memory.
 *
 * @Author Crt Valentincic <crt.valentincic@redpitaya.com>
 *         
 * (c) Red Pitaya  http://www.redpitaya.com
 *
 * This part of code is written in C programming language.
 * Please visit http://en.wikipedia.org/wiki/C_(programming_language)
 * for more details on the language used herein.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
//#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdint.h>

//#include "redpitaya/version.h"

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
  __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)

#define USAGE "Usage: \n\treader 0x4xxxxxxx ...\n"

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

unsigned long argv_at(
  int a_argc, char **a_argv,   // command line args
  int a_argi                  // current argument index
  ) ;

uint32_t read_value(uint32_t a_addr);

const int DEBUG = 0;

void* map_base = (void*)(-1);

int main(int argc, char **argv) {
	int fd = -1;
	int retval = EXIT_SUCCESS;

	if(argc < 2) {
		fprintf(stderr, USAGE );
		return EXIT_FAILURE;
	}

	if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) FATAL;

	/* Read from command line */
	unsigned long addr;
	



  for (int i=1; i<argc; i++) {
    addr = argv_at(argc, argv, i);
    if (DEBUG > 0) fprintf(stderr, "Addr: %08lx\n", addr );

    if (addr != 0) {
      /* Map one page */
      map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, addr & ~MAP_MASK);
      if(map_base == (void *) -1) FATAL;
      
      read_value( addr );

      if (map_base != (void*)(-1)) {
        if(munmap(map_base, MAP_SIZE) == -1) FATAL;
        map_base = (void*)(-1);
      }

      if (map_base != (void*)(-1)) {
        if(munmap(map_base, MAP_SIZE) == -1) FATAL;
      }
    }
	  fflush(stdout);
  }


	if (fd != -1) {
		close(fd);
	}
	
	return retval;
}

uint32_t read_value(uint32_t a_addr) {
	void* virt_addr = map_base + (a_addr & MAP_MASK);
	uint32_t read_result = 0;
	read_result = *((uint32_t *) virt_addr);
	printf("0x%08x\n", read_result);
//	fflush(stdout);
	return read_result;
}

unsigned long argv_at(
  int a_argc, char **a_argv,   // command line args
  int a_argi                  // current argument index
//  unsigned long* a_addr \       // resulting address
  ) {
	  if (DEBUG > 0) fprintf(stderr, "argi: %d %d %s\n", a_argc, a_argi, a_argv[a_argi]);
    if (a_argi>0 && a_argi<a_argc){
	    return strtoul(a_argv[a_argi], 0, 0);
	  }else{
	    return 0;
	  }
  }

